import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from './service/student-service';
import { StudentDataImplService } from './service/student-data-impl.service';
import {StudentDataImpl02Service} from './service/student-data-impl02.service';




@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
      {provide: StudentService, useClass: StudentDataImpl02Service}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
