import { Observable, of } from "rxjs";
import { Student } from "../entity/student";

export abstract class StudentService {
    abstract getStudents() : Observable<Student[]>;
    
}
