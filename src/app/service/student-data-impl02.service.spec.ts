import { TestBed, inject } from '@angular/core/testing';

import { StudentDataImpl02Service } from './student-data-impl02.service';

describe('StudentDataImpl02Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentDataImpl02Service]
    });
  });

  it('should be created', inject([StudentDataImpl02Service], (service: StudentDataImpl02Service) => {
    expect(service).toBeTruthy();
  }));
});
