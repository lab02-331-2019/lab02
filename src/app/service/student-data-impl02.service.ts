import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable, of } from 'rxjs';
import { Student } from '../entity/student';

@Injectable({
  providedIn: 'root'
})
export class StudentDataImpl02Service extends StudentService{

  constructor() { 
    super()
  }

getStudents(): Observable<Student[]>{
  return of(this.students);
};

students: Student[] = [{
  id: 1,
  studentId: '602115024',
  name: 'Cucumber',
  surname: 'Rot',
  gpa: 2.99
}, {
  id: 2,
  studentId: '602115028',
  name: 'Ooo',
  surname: 'Yeah',
  gpa: 3.22
}];}
